﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab_Calculator_CICD
{
    public class Calculator
    {
        public int add(int first_Number, int second_Number)
        {
            int added_Number = 0;

            added_Number = first_Number + second_Number;

            Console.WriteLine("Total number is " + added_Number);

            return added_Number;

        }

        public int sub(int first_Number, int second_Number)
        {
            int sub_Number = 0;

            sub_Number = first_Number - second_Number;

            Console.WriteLine("Total number is " + sub_Number);

            return sub_Number;

        }
    }
}
