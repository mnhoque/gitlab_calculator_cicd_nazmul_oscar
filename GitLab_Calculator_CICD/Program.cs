﻿using System;

namespace GitLab_Calculator_CICD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What do you want, add or sub");

            string entered_Number = Console.ReadLine();

            if (entered_Number == "add")
            {
                Console.WriteLine("Enter the first int number ");
                int first_Number = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter the second int number ");
                int second_Number = int.Parse(Console.ReadLine());
                Calculator calculator = new Calculator();
                calculator.add(first_Number, second_Number);
            }

            //Console.WriteLine("Hello World!");

            if (entered_Number == "sub")
            {
                int sub_Number = 0;
                Console.WriteLine("Enter the first int number ");
                int first_Number = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter the second int number ");
                int second_Number = int.Parse(Console.ReadLine());

                if (first_Number >= second_Number)
                {
                    sub_Number = first_Number - second_Number;
                }
                else
                {
                    Console.WriteLine("Enter the first number again");
                }


                Console.WriteLine("Total number is " + sub_Number);
            }

            Console.ReadKey();
        }
    }
}
