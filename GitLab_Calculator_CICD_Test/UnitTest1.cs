using System;
using Xunit;
using GitLab_Calculator_CICD;

namespace GitLab_Calculator_CICD_Test
{
    public class UnitTest1
    {
        [Fact]
        public void add()
        {
            Calculator calculator = new Calculator();
            int Value_1 = 5;
            int Value_2 = 6;
            int expected = 11;
            int actual = calculator.add(Value_1, Value_2);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void sub()
        {
            Calculator calculator = new Calculator();
            int Value_1 = 7;
            int Value_2 = 6;
            int expected = 1;
            int actual = calculator.sub(Value_1, Value_2);

            Assert.Equal(expected, actual);
        }
    }
}
